pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven {
            name = "mathlibdemomaven"
            url = uri("https://stagetruemeds.jfrog.io/artifactory/mathlibdemomaven/") // Change to your Artifactory URL
        }

    }
}

rootProject.name = "DemoLibApp"
include(":app")
include(":mathscalculation")
